package com.example.user.scandoc.ui.start;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.user.scandoc.R;
import com.example.user.scandoc.ui.base.BaseActivity;
import com.example.user.scandoc.ui.login.LoginActivity;
import com.example.user.scandoc.ui.main.MainActivity;

public class StartActivity extends BaseActivity implements StartMvpView {

    StartMvpPresenter<StartMvpView> presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        presenter = new StartPresenter<>(this);

        presenter.onAttach(StartActivity.this);
        presenter.isLogin();

    }

    @Override
    public void openLoginActivity() {
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    @Override
    public void openMainActivity() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}
