package com.example.user.scandoc.ui.main.adapets;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.user.scandoc.R;

import java.util.List;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.ViewHolder> {

    Callback callback;
    List<Item> list;

    public MainAdapter(List<Item> list) {
        this.list = list;
    }

    public MainAdapter(Callback callback, List<Item> list) {
        this.callback = callback;
        this.list = list;
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_view_item,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
//        viewHolder.tvTitle.setText("Низоли оила");
//        viewHolder.tvBody.setText("12 та чора тадбир туркумда 40 та одам \n2 та тадбирнинг муддати яқинлашган");
        viewHolder.tvTitle.setText(list.get(i).title);
        viewHolder.tvBody.setText(list.get(i).body);
        final int position = i;
        if (list.get(i).isred){
            viewHolder.imageView.setImageResource(R.drawable.round_red_image);
        } else {
            viewHolder.imageView.setImageResource(R.drawable.round_grey_image);
        }
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.clickRecyclerItem(position);
            }
        });

    }
    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView tvTitle;
        TextView tvBody;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.id_recycler_image);
            tvTitle = itemView.findViewById(R.id.recycler_text_title);
            tvBody = itemView.findViewById(R.id.recycler_text_body);
        }
    }
}
