package com.example.user.scandoc.ui.main.fragments.create_action;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.user.scandoc.R;
import com.example.user.scandoc.ui.main.fragments.mahalla.adapter.AdapterMahalla;

import java.util.List;

class AdapterCreateAction extends RecyclerView.Adapter<AdapterCreateAction.ViewHolder> {

    boolean [] booleans = new boolean[10];
    List<ActionCitizen> selectlist;
    Calback calback;


    public void setSelectlist(List<ActionCitizen> selectlist) {
        this.selectlist = selectlist;
    }

    public AdapterCreateAction(Calback calback) {
        this.calback = calback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_select_citezen,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {
        if (selectlist != null){
            viewHolder.title.setText(selectlist.get(i).title);
            viewHolder.body.setText(selectlist.get(i).body);
        }else {
            viewHolder.title.setText("VAlixonov BAxtiyor");
            viewHolder.body.setText("Vilovot Tuman qishloq");
        }
        final int position = i;
        if (booleans[position]){
            viewHolder.checkBox.setChecked(true);
        }else {
            viewHolder.checkBox.setChecked(false);
        }

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewHolder.checkBox.isChecked()){
                    viewHolder.checkBox.setChecked(false);
                    booleans[position] = false;
                    calback.removeItem(selectlist.get(position));
                }else {
                    viewHolder.checkBox.setChecked(true);
                    booleans[position] = true;
                    calback.addItem(selectlist.get(position));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return 10;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView title, body;
        CheckBox checkBox;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.title);
            body = itemView.findViewById(R.id.body);
            checkBox = itemView.findViewById(R.id.checkBox);
        }
    }
}
