package com.example.user.scandoc.ui.main.fragments.dashboatd;

import android.content.Context;

import com.example.user.scandoc.ui.base.BasePresenter;

public class DashboardPresenter<V extends DashboardMvpView> extends BasePresenter<V> implements DashboardMvpPresenter<V>{
    public DashboardPresenter(Context context) {
        super(context);
    }
}
