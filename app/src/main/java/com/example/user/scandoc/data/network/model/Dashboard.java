package com.example.user.scandoc.data.network.model;

public class Dashboard {

    Meta meta;
    Payload payload;

    public Dashboard() {
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public Payload getPayload() {
        return payload;
    }

    public void setPayload(Payload payload) {
        this.payload = payload;
    }

    @Override
    public String toString() {
        return "Payload{" +
                "meta=" + meta +
                ", payload=" + payload +
                '}';
    }

}
