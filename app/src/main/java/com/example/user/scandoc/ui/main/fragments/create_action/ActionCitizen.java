package com.example.user.scandoc.ui.main.fragments.create_action;

public class ActionCitizen {

    String title;
    String body;

    public ActionCitizen() {
    }

    public ActionCitizen(int i) {
        title = "Камолов " + i;
        body = "Саид " + i;

    }

    public ActionCitizen(String title, String body) {
        this.title = title;
        this.body = body;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
