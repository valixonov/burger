package com.example.user.scandoc.ui.main.fragments.mahalla;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.user.scandoc.R;
import com.example.user.scandoc.ui.base.BaseFragment;
import com.example.user.scandoc.ui.main.FragmentListener;
import com.example.user.scandoc.ui.main.fragments.mahalla.adapter.AdapterMahalla;
import com.example.user.scandoc.ui.main.fragments.mahalla.adapter.Callback;

public class MahallaFragment extends BaseFragment implements MahallaMvpView, Callback {

    MahallaMvpPresenter<MahallaMvpView> presenter;

    FragmentListener listener;

    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;

    public MahallaFragment() {
    }

    public static MahallaFragment newInstance() {
        MahallaFragment fragment = new MahallaFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_mahalla, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        presenter = new MahallaPresenter<>(getContext());
        presenter.onAttach(this);

        adapter = new AdapterMahalla(this);
        recyclerView = view.findViewById(R.id.recycler_view_mahalla);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void openCitizen() {
        listener.openCitezenFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentListener){
            listener = (FragmentListener) context;
        }else {
            throw new RuntimeException(context.toString() + "must implement listener");
        }
    }
}
