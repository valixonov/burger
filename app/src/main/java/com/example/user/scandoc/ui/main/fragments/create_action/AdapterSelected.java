package com.example.user.scandoc.ui.main.fragments.create_action;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.user.scandoc.R;

import java.util.List;

class AdapterSelected extends RecyclerView.Adapter<AdapterSelected.ViewHolder> {

    boolean [] booleans = new boolean[10];
    List<ActionCitizen> selectlist;


    public void setSelectlist(List<ActionCitizen> selectlist) {
        this.selectlist = selectlist;
    }

    public AdapterSelected() {
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_select_citezen,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {
        if (selectlist != null){
            viewHolder.title.setText(selectlist.get(i).title);
            viewHolder.body.setText(selectlist.get(i).body);
        }else {
            viewHolder.title.setText("VAlixonov BAxtiyor");
            viewHolder.body.setText("Vilovot Tuman qishloq");
        }
        viewHolder.checkBox.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {

        if (selectlist != null){
            return selectlist.size();
        }else return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView title, body;
        CheckBox checkBox;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.title);
            body = itemView.findViewById(R.id.body);
            checkBox = itemView.findViewById(R.id.checkBox);
        }
    }
}
