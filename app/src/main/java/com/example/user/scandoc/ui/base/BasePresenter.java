package com.example.user.scandoc.ui.base;

import android.content.Context;

import com.example.user.scandoc.data.AppDataManager;
import com.example.user.scandoc.data.DataManager;

public class BasePresenter<V extends MvpView> implements MvpPresenter<V> {
    private V mMvpView;
    private DataManager dataManager;


    public BasePresenter(Context context){
        dataManager = new AppDataManager(context);
    }

    public DataManager getDataManager() {
        return dataManager;
    }

    @Override
    public void onAttach(V mvpView) {
        mMvpView = mvpView;
    }

    @Override
    public void onDetach() {
        mMvpView = null;
    }

    public V getMvpView(){
        return mMvpView;
    }

    public boolean isViewAttached(){
        return mMvpView != null;
    }
}
