package com.example.user.scandoc.ui.start;

import com.example.user.scandoc.ui.base.MvpView;

public interface StartMvpView extends MvpView {

    void openLoginActivity();
    void openMainActivity();
}
