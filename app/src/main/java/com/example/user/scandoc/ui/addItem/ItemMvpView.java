package com.example.user.scandoc.ui.addItem;

import com.example.user.scandoc.ui.base.MvpView;

public interface ItemMvpView extends MvpView {
    void openMainActivity();
}
