package com.example.user.scandoc.ui.addItem;

import com.example.user.scandoc.data.network.model.TurkumList;
import com.example.user.scandoc.data.network.model.User;
import com.example.user.scandoc.ui.base.MvpPresenter;

import java.util.List;

public interface ItemMvpPresenter<V extends ItemMvpView> extends MvpPresenter<V> {
    List<User> getUsers();

    List<TurkumList> getTurkumLists();
}
