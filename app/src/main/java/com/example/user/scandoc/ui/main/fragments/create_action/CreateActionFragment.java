package com.example.user.scandoc.ui.main.fragments.create_action;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.example.user.scandoc.R;
import com.example.user.scandoc.data.network.model.Action;
import com.example.user.scandoc.ui.base.BaseFragment;
import com.example.user.scandoc.ui.main.FragmentListener;
import com.example.user.scandoc.ui.main.fragments.dashboatd.DashboardFragment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class CreateActionFragment extends BaseFragment implements CreateActionMvpView, Calback {

    CreateActionMvpPresenter<CreateActionMvpView> presenter;
    FragmentListener listener;

    Toolbar toolbar;
    ImageView back;
    RelativeLayout input_layout;
    RecyclerView recyclerView;
    RecyclerView recyclerSelected;
    AdapterCreateAction adapter;
    AdapterSelected adapterSelected;
    Button select_taxo_but, select_citizen_button, a_commit_date;
    FloatingActionButton fab;

    private int mYear, mMonth, mDay;

    List<ActionCitizen> selectlist;
    List<ActionCitizen> selectedlist;

    AlertDialog.Builder dialog;
    boolean[] ischeck;

    public CreateActionFragment() {
    }

    public static CreateActionFragment newInstance() {
        CreateActionFragment fragment = new CreateActionFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_create_action, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        presenter = new CreateActionPresenter<>(getContext());
        presenter.onAttach(this);

        select_taxo_but = view.findViewById(R.id.select_taxo_term);
        select_citizen_button = view.findViewById(R.id.select_citizen);
        a_commit_date = view.findViewById(R.id.a_commit_date);
        input_layout = view.findViewById(R.id.input_layout);
        recyclerView = view.findViewById(R.id.select_citizen_recycler);
        recyclerSelected = view.findViewById(R.id.selected_citizen);

        fab = view.findViewById(R.id.fab);
        selectlist = new ArrayList<>();

        createCitezen();


        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        recyclerSelected.setLayoutManager(new LinearLayoutManager(view.getContext()));

        adapter = new AdapterCreateAction(this);
        adapterSelected = new AdapterSelected();
        adapter.setSelectlist(selectlist);
        recyclerView.setAdapter(adapter);
        recyclerSelected.setAdapter(adapterSelected);

        createDialog(view);



        select_citizen_button.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onClick(View v) {
                if (recyclerView.getVisibility() == View.GONE){
                    recyclerView.setVisibility(View.VISIBLE);
                    fab.setVisibility(View.VISIBLE);
                    input_layout.setVisibility(View.GONE);
                }else {
                    recyclerView.setVisibility(View.GONE);
                    fab.setVisibility(View.GONE);
                    input_layout.setVisibility(View.VISIBLE);
                }
            }
        });


        select_taxo_but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show();
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onClick(View v) {
                recyclerView.setVisibility(View.GONE);
                fab.setVisibility(View.GONE);
                input_layout.setVisibility(View.VISIBLE);
            }
        });

        a_commit_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCalendar();
            }
        });

        android.text.format.DateFormat df = new android.text.format.DateFormat();
        a_commit_date.setText(df.format("dd-MM-yyyy", new java.util.Date()).toString());

    }

    private void createCitezen() {
        ActionCitizen citizen1 = new ActionCitizen("Рашидов Камол Бердиевич", "Бойсун туман, Инкабод");
        ActionCitizen citizen2 = new ActionCitizen("Нуров Азамат Шарифович", "Бойсун туман, Даштиғоз");
        ActionCitizen citizen3 = new ActionCitizen("Болтаев Валижон Шавкатович", "Бойсун туман, Пудина");
        ActionCitizen citizen4 = new ActionCitizen("Холов Акмал Солиевич", "Бойсун туман, Пулҳоким");
        ActionCitizen citizen5 = new ActionCitizen("Шакарова Малика Нурбековна", "Бойсун туман, Обикор");
        ActionCitizen citizen6 = new ActionCitizen("Саидов Шербек Мадалиевич", "Бойсун туман, Қудуқсой");
        ActionCitizen citizen7 = new ActionCitizen("Дадажонов Хўжаниёз Холович", "Бойсун туман, Газа");
        ActionCitizen citizen8 = new ActionCitizen("Суннатов Эшмамат Тешабоевиш", "Бойсун туман, Хўжаидод ");
        ActionCitizen citizen9 = new ActionCitizen("Махтумов Шастилло Каримович", "Бойсун туман, Омонхона");
        ActionCitizen citizen10 = new ActionCitizen("Шарипов Шухрат Камолович", "Бойсун туман, Омонхона қишлоғи");

        selectlist.add(citizen1);
        selectlist.add(citizen2);
        selectlist.add(citizen3);
        selectlist.add(citizen4);
        selectlist.add(citizen5);
        selectlist.add(citizen6);
        selectlist.add(citizen7);
        selectlist.add(citizen8);
        selectlist.add(citizen9);
        selectlist.add(citizen10);
       

    }

    private void openCalendar(){
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                String  date = dayOfMonth +"-0"+(month+1)+"-"+year;
                a_commit_date.setText(date);
            }
        },mYear, mMonth, mDay);
        dialog.show();
    }

    private void createDialog(View view) {
        dialog = new AlertDialog.Builder(view.getContext());
        dialog.setTitle("Астион холатини танланг");
        CharSequence[] charSequences = new CharSequence[10];
        ischeck = new boolean[10];
        for (int i = 0; i < 10; i++) {
            charSequences[i] = "холат" + i;
            ischeck[i] = false;
        }
        dialog.setMultiChoiceItems(charSequences, ischeck, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {

            }
        });
        dialog.setPositiveButton("Да", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        dialog.setNegativeButton("Нет", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        super.onAttach(context);
        if (context instanceof FragmentListener){
            listener = (FragmentListener) context;
        }else {
            throw new RuntimeException(context.toString() + "must implement listener");
        }
    }

    @Override
    public void addItem(ActionCitizen citizen) {
        selectedlist.add(citizen);
        adapterSelected.setSelectlist(selectedlist);
        adapterSelected.notifyDataSetChanged();
    }

    @Override
    public void removeItem(ActionCitizen citizen) {
        selectedlist.remove(citizen);
        adapterSelected.setSelectlist(selectedlist);
        adapterSelected.notifyDataSetChanged();
    }
}
