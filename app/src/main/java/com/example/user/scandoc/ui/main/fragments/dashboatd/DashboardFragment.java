package com.example.user.scandoc.ui.main.fragments.dashboatd;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.user.scandoc.R;
import com.example.user.scandoc.ui.base.BaseFragment;
import com.example.user.scandoc.ui.main.FragmentListener;
import com.example.user.scandoc.ui.main.adapets.Callback;
import com.example.user.scandoc.ui.main.adapets.Item;
import com.example.user.scandoc.ui.main.adapets.ItemAdapter;
import com.example.user.scandoc.ui.main.adapets.MainAdapter;

import java.util.ArrayList;

public class DashboardFragment extends BaseFragment implements DashboardMvpView, Callback {

    DashboardMvpPresenter<DashboardMvpView> presenter;
    FragmentListener listener;

    RecyclerView recyclerView;
    MainAdapter mainAdapter;
    ItemAdapter itemAdapter;

    boolean isBack = false;
    ArrayList<Item> turkumItems, turkum;

    public DashboardFragment() {
    }

    public static DashboardFragment newInstance() {
        DashboardFragment fragment = new DashboardFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_dashboard, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        presenter = new DashboardPresenter<>(getContext());
        presenter.onAttach(this);

//        createItems();
//        recyclerView = view.findViewById(R.id.recycler_view);
//        mainAdapter = new MainAdapter( listener.getCallback(), turkum);
//        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
//        recyclerView.setAdapter(mainAdapter);
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void clickRecyclerItem(int position) {
        listener.getToolbar().setTitle(R.string.title_activity_main_item);
        itemAdapter = new ItemAdapter(turkumItems);
        recyclerView.setAdapter(itemAdapter);
        listener.getFloatingActionButton().setVisibility(View.VISIBLE);
        isBack = true;
    }

    private void createItems() {
        Item item = new Item("Гиёҳвандлик", "4 та чора тадбир туркумда 16та одам \n2 та тадбирнинг муддати яқинлашган",false);
        Item item1 = new Item("Ичкиликка ружу қўйган", "10 та чора тадбир туркумда 35 та одам \n2 та тадбирнинг муддати яқинлашган",false);
        Item item2 = new Item("Низоли оила", "12 та чора тадбир туркумда 40та одам \n2 та тадбирнинг муддати яқинлашган",false);
        Item item3 = new Item("Суицид", "4 та чора тадбир туркумда 3та одам \n2 та тадбирнинг муддати яқинлашган",false);
        Item item4 = new Item("Руҳий касал", "4 та чора тадбир туркумда 16та одам \n2 та тадбирнинг муддати яқинлашган",false);
        turkum = new ArrayList<>();
        turkum.add(0, item);
        turkum.add(1, item1);
        turkum.add(2, item2);
        turkum.add(3, item3);
        turkum.add(4, item4);


        Item el = new Item("Шодиев Аскар, Норова гули", "Рашк \nҚўшимча: \nАдрес: Навоий кўчаси 8 уй",true);
        Item el1 = new Item("Собировлар", "Мерос \nҚўшимча: муқаддам судланган \nАдрес: Махтумқули 7 уй",true);
        Item el2 = new Item("Камолов Ботир, Турдиева Шодия", "Мол-мулк талашиш \nҚўшимча:Ажрашаетган ер \nАдрес:",false);
        Item el3 = new Item("Саидов Ж, Валиев Б", "Ер-Сув талашиш \nҚўшимча:қўшнилар \nАдрес: Низомий 7, Низомий 7",false);
        Item el4 = new Item("Ниёзова А, Жалилова А", "Мерос \nҚўшимча: Опа-сингил",false);
        turkumItems = new ArrayList<>();
        turkumItems.add(0, el);
        turkumItems.add(1, el1);
        turkumItems.add(2, el2);
        turkumItems.add(3, el3);
        turkumItems.add(4, el4);

    }
}
