package com.example.user.scandoc.ui.main.adapets;

public class Item {
    String title;
    String body;
    boolean isred;

    public Item() {
    }

    public Item(String title, String body, boolean isred) {
        this.title = title;
        this.body = body;
        this.isred = isred;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public boolean isIsred() {
        return isred;
    }

    public void setIsred(boolean isred) {
        this.isred = isred;
    }
}
