package com.example.user.scandoc.ui.addItem;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.TextView;

import com.example.user.scandoc.R;
import com.example.user.scandoc.ui.base.BaseActivity;

//import okhttp3.OkHttpClient;

public class ItemActivity extends BaseActivity implements ItemMvpView {

    ItemMvpPresenter<ItemMvpView> presenter;
    TextView textView;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);

        init();
    }

    private void init() {
        presenter = new ItemPresenter<>(this);
        presenter.onAttach(ItemActivity.this);
        textView = findViewById(R.id.id_respons_text);
        //presenter.getUsers();
        presenter.getTurkumLists();
        //textView.setText(presenter.getUsers().get(0).getName());
        //OkHttpClient client;
    }

    @Override
    public void openMainActivity() {

    }
}
