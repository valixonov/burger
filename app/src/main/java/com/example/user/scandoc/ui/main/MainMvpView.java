package com.example.user.scandoc.ui.main;

import com.example.user.scandoc.ui.base.MvpView;

public interface MainMvpView extends MvpView {

    void openLoginActivity();
    void openItemActivity();
}
