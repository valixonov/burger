package com.example.user.scandoc.data.network.model;

public class ActionTaxo {

    private String a_t_id;
    private String a_t_label;
    private String a_tt_parent_id;
    private String a_tt_parent_label;
    private String a_tt_id;
    private String a_tt_label;

    public ActionTaxo() {
    }

    public String getA_t_id() {
        return a_t_id;
    }

    public void setA_t_id(String a_t_id) {
        this.a_t_id = a_t_id;
    }

    public String getA_t_label() {
        return a_t_label;
    }

    public void setA_t_label(String a_t_label) {
        this.a_t_label = a_t_label;
    }

    public String getA_tt_parent_id() {
        return a_tt_parent_id;
    }

    public void setA_tt_parent_id(String a_tt_parent_id) {
        this.a_tt_parent_id = a_tt_parent_id;
    }

    public String getA_tt_parent_label() {
        return a_tt_parent_label;
    }

    public void setA_tt_parent_label(String a_tt_parent_label) {
        this.a_tt_parent_label = a_tt_parent_label;
    }

    public String getA_tt_id() {
        return a_tt_id;
    }

    public void setA_tt_id(String a_tt_id) {
        this.a_tt_id = a_tt_id;
    }

    public String getA_tt_label() {
        return a_tt_label;
    }

    public void setA_tt_label(String a_tt_label) {
        this.a_tt_label = a_tt_label;
    }

    @Override
    public String toString() {
        return "ActionTaxo{" +
                "a_t_id='" + a_t_id + '\'' +
                ", a_t_label='" + a_t_label + '\'' +
                ", a_tt_parent_id='" + a_tt_parent_id + '\'' +
                ", a_tt_parent_label='" + a_tt_parent_label + '\'' +
                ", a_tt_id='" + a_tt_id + '\'' +
                ", a_tt_label='" + a_tt_label + '\'' +
                '}';
    }
}
