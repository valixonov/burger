package com.example.user.scandoc.ui.main.fragments.create_action;

import android.content.Context;

import com.example.user.scandoc.ui.base.BasePresenter;
import com.example.user.scandoc.ui.base.MvpView;

public class CreateActionPresenter<V extends MvpView> extends BasePresenter<V> implements CreateActionMvpPresenter<V> {
    public CreateActionPresenter(Context context) {
        super(context);
    }
}
