package com.example.user.scandoc.ui.main;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;

import com.example.user.scandoc.ui.main.adapets.Callback;

public interface FragmentListener {

    Callback getCallback();

    Toolbar getToolbar();

    FloatingActionButton getFloatingActionButton();

    void openCitezenFragment();

    void changedToolbar(Toolbar toolbar);

}
