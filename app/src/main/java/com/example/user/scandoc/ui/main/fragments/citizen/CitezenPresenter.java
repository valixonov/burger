package com.example.user.scandoc.ui.main.fragments.citizen;

import android.content.Context;

import com.example.user.scandoc.ui.base.BasePresenter;
import com.example.user.scandoc.ui.main.fragments.mahalla.MahallaMvpPresenter;
import com.example.user.scandoc.ui.main.fragments.mahalla.MahallaMvpView;

public class CitezenPresenter<V extends CitezenMvpView> extends BasePresenter<V> implements CitezenMvpPresenter<V> {
    public CitezenPresenter(Context context) {
        super(context);
    }
}
