package com.example.user.scandoc.ui.base;

public interface MvpView {

    void showLoading();

    void hideLoading();

    void showMessage(String message);
}
