package com.example.user.scandoc.ui.main.fragments.mahalla.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.user.scandoc.R;

public class AdapterMahalla extends RecyclerView.Adapter<AdapterMahalla.ViewHolder> {

    Callback callback;

    public AdapterMahalla(Callback callback) {
        this.callback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_view_item,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        if (i == 0){
            viewHolder.tvTitle.setText("Олмазор");
        } else  if (i == 1){
            viewHolder.tvTitle.setText("Гул Тепа");
        } else  if (i == 2){
            viewHolder.tvTitle.setText("Обод махалла");
        }
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.openCitizen();
            }
        });

    }

    @Override
    public int getItemCount() {
        return 3;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView tvTitle;
        TextView tvBody;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.id_recycler_image);
            tvTitle = itemView.findViewById(R.id.recycler_text_title);
            tvBody = itemView.findViewById(R.id.recycler_text_body);
        }
    }
}
