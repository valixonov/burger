package com.example.user.scandoc.ui.main.fragments.dashboatd;

import com.example.user.scandoc.ui.base.BasePresenter;
import com.example.user.scandoc.ui.base.MvpPresenter;
import com.example.user.scandoc.ui.base.MvpView;

public interface DashboardMvpPresenter<V extends DashboardMvpView> extends MvpPresenter<V> {
}
