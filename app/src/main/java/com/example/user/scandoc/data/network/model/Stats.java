package com.example.user.scandoc.data.network.model;

public class Stats {

    private String total;

    public Stats() {
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "Stats{" +
                "total='" + total + '\'' +
                '}';
    }
}
