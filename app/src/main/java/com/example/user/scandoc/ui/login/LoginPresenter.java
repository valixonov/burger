package com.example.user.scandoc.ui.login;

import android.content.Context;

import com.example.user.scandoc.ui.base.BasePresenter;
import com.example.user.scandoc.ui.base.MvpView;

public class LoginPresenter<V extends LoginMvpView> extends BasePresenter<V> implements LoginMvpPresenter<V>{

    public LoginPresenter(Context context) {
        super(context);
    }

    @Override
    public void onLoginClick(String login, String password) {
        if (login.isEmpty() || password.isEmpty()){
            getMvpView().showMessage("Илтимос Логин ёки Парол киритинг!");
        }else {
            getDataManager().setLoggedIn();
            getMvpView().openMainActivity();
        }
    }


}
