package com.example.user.scandoc.data;

import android.content.Context;

//import com.example.user.scandoc.data.network.ApiClient;
//import com.example.user.scandoc.data.network.ApiService;
import com.example.user.scandoc.data.network.ApiClient;
import com.example.user.scandoc.data.network.model.TurkumList;
import com.example.user.scandoc.data.network.model.User;
import com.example.user.scandoc.data.prefs.AppPreferencesHelper;
import com.example.user.scandoc.data.prefs.PreferencesHelper;

import java.util.List;

import retrofit2.Call;

import retrofit2.Call;

public class AppDataManager implements DataManager{

    private PreferencesHelper mPreferencesHelper;

//    private ApiService apiService;
    private ApiClient client;

    public AppDataManager(Context context) {
        mPreferencesHelper = new AppPreferencesHelper(context);
        //apiService = ApiClient.getRetrofit().create(ApiService.class);
        client = new ApiClient(context);
    }

    @Override
    public void setLoggedIn() {
        mPreferencesHelper.setLoggedIn();
    }

    @Override
    public boolean isLoggedIn() {
        return mPreferencesHelper.isLoggedIn();
    }

    @Override
    public void setLogOut() {
        mPreferencesHelper.setLogOut();
    }

//    @Override
//    public Call<List<User>> reposForUser(String user) {
//        return null;
//    }
//
    public List<User> getUsers() {
        return client.getUsers();
    }

    public List<TurkumList> getTurkumLists(){
        return client.getTurkumLists();
    }
    //    @Override
//    public Call<List<TurkumList>> responListCall() {
//       return null;
//        // return apiService.responListCall();
//    }
}
