package com.example.user.scandoc.data.network;

import com.example.user.scandoc.data.network.model.TurkumList;
import com.example.user.scandoc.data.network.model.User;

import java.util.List;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ApiService {
//    @GET(ApiEndPoint.GET_LIST)
//    Call<List<TurkumList>> responListCall();
@GET("/users/{user}/repos")
Call<List<User>> reposForUser(@Path("user") String user);

@GET("/get")
Call<List<TurkumList>> reposForTur();
}
