package com.example.user.scandoc.ui.main.fragments.citizen.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.user.scandoc.R;

public class AdapterCitezen extends RecyclerView.Adapter<AdapterCitezen.ViewHolder> {
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_view_item,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        if (i == 0){
            viewHolder.tvTitle.setText("Рашидов Камол Бердиевич");
            viewHolder.tvBody.setText("Бойсун туман, Инкабод");
        } else  if (i == 1){
            viewHolder.tvTitle.setText("Нуров Азамат Шарифович");
            viewHolder.tvBody.setText("Бойсун туман, Даштиғоз");
        } else  if (i == 2){
            viewHolder.tvTitle.setText("Болтаев Валижон Шавкатович");
            viewHolder.tvBody.setText("Бойсун туман, Пудина");
        } else  if (i == 3){
            viewHolder.tvTitle.setText("Холов Акмал Солиевич");
            viewHolder.tvBody.setText("Бойсун туман, Пулҳоким");
        } else  if (i == 4){
            viewHolder.tvTitle.setText("Шакарова Малика Нурбековна");
            viewHolder.tvBody.setText("Бойсун туман, Обикор");
        } else  if (i == 5){
            viewHolder.tvTitle.setText("Саидов Шербек Мадалиевич");
            viewHolder.tvBody.setText("Бойсун туман, Қудуқсой");
        } else  if (i == 6){
            viewHolder.tvTitle.setText("Дадажонов Хўжаниёз Холович");
            viewHolder.tvBody.setText("Бойсун туман, Газа");
        } else  if (i == 7){
            viewHolder.tvTitle.setText("Суннатов Эшмамат Тешабоевиш");
            viewHolder.tvBody.setText("Бойсун туман, Хўжаидод ");
        } else  if (i == 8){
            viewHolder.tvTitle.setText("Махтумов Шастилло Каримович");
            viewHolder.tvBody.setText("Бойсун туман, Омонхона");
        } else  if (i == 9){
            viewHolder.tvTitle.setText("Шарипов Шухрат Камолович");
            viewHolder.tvBody.setText("Бойсун туман, Омонхона қишлоғи");
        }

    }

    @Override
    public int getItemCount() {
        return 10;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView tvTitle;
        TextView tvBody;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.id_recycler_image);
            tvTitle = itemView.findViewById(R.id.recycler_text_title);
            tvBody = itemView.findViewById(R.id.recycler_text_body);
        }
    }
}
