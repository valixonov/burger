package com.example.user.scandoc.data.prefs;

public interface PreferencesHelper {
    public void setLoggedIn();

    boolean isLoggedIn();

    public void setLogOut();
}
