package com.example.user.scandoc.data;

import com.example.user.scandoc.data.network.ApiService;
import com.example.user.scandoc.data.network.model.TurkumList;
import com.example.user.scandoc.data.network.model.User;
import com.example.user.scandoc.data.prefs.PreferencesHelper;

import java.util.List;

public interface DataManager extends PreferencesHelper {
    List<User> getUsers();

    List<TurkumList> getTurkumLists();
}
