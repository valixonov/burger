package com.example.user.scandoc.ui.main.fragments.citizen;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.user.scandoc.R;
import com.example.user.scandoc.ui.base.BaseFragment;
import com.example.user.scandoc.ui.main.fragments.citizen.adapter.AdapterCitezen;
import com.example.user.scandoc.ui.main.fragments.mahalla.MahallaFragment;
import com.example.user.scandoc.ui.main.fragments.mahalla.MahallaMvpPresenter;
import com.example.user.scandoc.ui.main.fragments.mahalla.MahallaMvpView;
import com.example.user.scandoc.ui.main.fragments.mahalla.MahallaPresenter;
import com.example.user.scandoc.ui.main.fragments.mahalla.adapter.AdapterMahalla;

public class CitizenFragment extends BaseFragment implements CitezenMvpView{

    CitezenMvpPresenter<CitezenMvpView> presenter;

    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;

    public CitizenFragment() {
    }

    public static MahallaFragment newInstance() {
        MahallaFragment fragment = new MahallaFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_citezen, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        presenter = new CitezenPresenter<>(getContext());
        presenter.onAttach(this);

        adapter = new AdapterCitezen();
        recyclerView = view.findViewById(R.id.recycler_view_citezen);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
    }
}
