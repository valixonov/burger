package com.example.user.scandoc.ui.main.fragments.mahalla;

import com.example.user.scandoc.ui.base.MvpPresenter;
import com.example.user.scandoc.ui.base.MvpView;

public interface MahallaMvpPresenter<V extends MahallaMvpView> extends MvpPresenter<V> {
}
