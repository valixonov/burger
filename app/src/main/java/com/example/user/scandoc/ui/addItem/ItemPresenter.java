package com.example.user.scandoc.ui.addItem;

import android.content.Context;

import com.example.user.scandoc.data.network.model.TurkumList;
import com.example.user.scandoc.data.network.model.User;
import com.example.user.scandoc.ui.base.BasePresenter;

import java.util.List;

public class ItemPresenter<V extends ItemMvpView> extends BasePresenter<V> implements ItemMvpPresenter<V> {
    public ItemPresenter(Context context) {
        super(context);
    }

    @Override
    public List<User> getUsers() {
        return getDataManager().getUsers();
    }

    @Override
    public List<TurkumList> getTurkumLists() {
        return getDataManager().getTurkumLists();
    }
}
