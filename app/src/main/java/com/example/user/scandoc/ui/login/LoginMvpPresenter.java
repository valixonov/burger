package com.example.user.scandoc.ui.login;

import com.example.user.scandoc.ui.base.MvpPresenter;

public interface LoginMvpPresenter<V extends LoginMvpView> extends MvpPresenter<V> {
    void onLoginClick(String login, String password);
}
