package com.example.user.scandoc.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.user.scandoc.R;
import com.example.user.scandoc.ui.base.BaseActivity;
import com.example.user.scandoc.ui.main.MainActivity;

public class LoginActivity extends BaseActivity implements LoginMvpView, View.OnClickListener {

    private LoginMvpPresenter<LoginMvpView> presenter;
    private EditText etLogin, etParol;
    private Button tvLogin;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        init();

    }

    private void init() {
        presenter = new LoginPresenter<>(this);
        presenter.onAttach(LoginActivity.this);
        etLogin = findViewById(R.id.etLogin);
        etParol = findViewById(R.id.etParol);
        tvLogin = findViewById(R.id.tvLogin);
        tvLogin.setOnClickListener(this);
    }

    @Override
    public void openMainActivity() {
        startActivity(new Intent(this,MainActivity.class));
        finish();
    }

    @Override
    public void onClick(View v) {
        String  login = etLogin.getText().toString();
        String  parol = etParol.getText().toString();
        presenter.onLoginClick(login, parol);
    }


}
