package com.example.user.scandoc.ui.start;

import android.content.Context;

import com.example.user.scandoc.ui.base.BasePresenter;

public class StartPresenter<V extends StartMvpView> extends BasePresenter<V> implements StartMvpPresenter<V>{

    public StartPresenter(Context context) {
        super(context);
    }

    @Override
    public void isLogin() {
        if (getDataManager().isLoggedIn()){
            getMvpView().openMainActivity();
        }else getMvpView().openLoginActivity();
    }
}
