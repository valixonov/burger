package com.example.user.scandoc.ui.main.fragments.mahalla;

import android.content.Context;

import com.example.user.scandoc.ui.base.BasePresenter;

public class MahallaPresenter<V extends MahallaMvpView> extends BasePresenter<V> implements MahallaMvpPresenter<V> {
    public MahallaPresenter(Context context) {
        super(context);
    }
}
