package com.example.user.scandoc.data.network.model;

public class Citizen {

    private String c_id;
    private String c_rel_id;
    private String c_fullname;
    private String c_address;
    private String c_address_landmark;
    private String c_dob;
    private String c_t_count;
    private Doc c_i_docs;
    private CTaxo c_t;


    public Citizen() {
    }

    public String getC_id() {
        return c_id;
    }

    public void setC_id(String c_id) {
        this.c_id = c_id;
    }

    public String getC_rel_id() {
        return c_rel_id;
    }

    public void setC_rel_id(String c_rel_id) {
        this.c_rel_id = c_rel_id;
    }

    public String getC_fullname() {
        return c_fullname;
    }

    public void setC_fullname(String c_fullname) {
        this.c_fullname = c_fullname;
    }

    public String getC_address() {
        return c_address;
    }

    public void setC_address(String c_address) {
        this.c_address = c_address;
    }

    public String getC_address_landmark() {
        return c_address_landmark;
    }

    public void setC_address_landmark(String c_address_landmark) {
        this.c_address_landmark = c_address_landmark;
    }

    public String getC_dob() {
        return c_dob;
    }

    public void setC_dob(String c_dob) {
        this.c_dob = c_dob;
    }

    public String getC_t_count() {
        return c_t_count;
    }

    public void setC_t_count(String c_t_count) {
        this.c_t_count = c_t_count;
    }

    public Doc getC_i_docs() {
        return c_i_docs;
    }

    public void setC_i_docs(Doc c_i_docs) {
        this.c_i_docs = c_i_docs;
    }

    public CTaxo getC_t() {
        return c_t;
    }

    public void setC_t(CTaxo c_t) {
        this.c_t = c_t;
    }

    @Override
    public String toString() {
        return "Citizen{" +
                "c_id='" + c_id + '\'' +
                ", c_rel_id='" + c_rel_id + '\'' +
                ", c_fullname='" + c_fullname + '\'' +
                ", c_address='" + c_address + '\'' +
                ", c_address_landmark='" + c_address_landmark + '\'' +
                ", c_dob='" + c_dob + '\'' +
                ", c_t_count='" + c_t_count + '\'' +
                ", c_i_docs=" + c_i_docs +
                ", c_t=" + c_t +
                '}';
    }

    private class Doc{

        private String c_i_doc_id;
        private String c_i_doc_type;
        private String c_i_doc_series;
        private String c_i_doc_status;

        public Doc() {
        }

        public String getC_i_doc_id() {
            return c_i_doc_id;
        }

        public void setC_i_doc_id(String c_i_doc_id) {
            this.c_i_doc_id = c_i_doc_id;
        }

        public String getC_i_doc_type() {
            return c_i_doc_type;
        }

        public void setC_i_doc_type(String c_i_doc_type) {
            this.c_i_doc_type = c_i_doc_type;
        }

        public String getC_i_doc_series() {
            return c_i_doc_series;
        }

        public void setC_i_doc_series(String c_i_doc_series) {
            this.c_i_doc_series = c_i_doc_series;
        }

        public String getC_i_doc_status() {
            return c_i_doc_status;
        }

        public void setC_i_doc_status(String c_i_doc_status) {
            this.c_i_doc_status = c_i_doc_status;
        }

        @Override
        public String toString() {
            return "Doc{" +
                    "c_i_doc_id='" + c_i_doc_id + '\'' +
                    ", c_i_doc_type='" + c_i_doc_type + '\'' +
                    ", c_i_doc_series='" + c_i_doc_series + '\'' +
                    ", c_i_doc_status='" + c_i_doc_status + '\'' +
                    '}';
        }
    }

    private class CTaxo{

        private String c_t_id;
        private String c_t_label;
        private String c_tt_parent_id;
        private String c_tt_parent_label;
        private String c_tt_id;
        private String c_tt_label;

        public CTaxo() {
        }

        public String getC_t_id() {
            return c_t_id;
        }

        public void setC_t_id(String c_t_id) {
            this.c_t_id = c_t_id;
        }

        public String getC_t_label() {
            return c_t_label;
        }

        public void setC_t_label(String c_t_label) {
            this.c_t_label = c_t_label;
        }

        public String getC_tt_parent_id() {
            return c_tt_parent_id;
        }

        public void setC_tt_parent_id(String c_tt_parent_id) {
            this.c_tt_parent_id = c_tt_parent_id;
        }

        public String getC_tt_parent_label() {
            return c_tt_parent_label;
        }

        public void setC_tt_parent_label(String c_tt_parent_label) {
            this.c_tt_parent_label = c_tt_parent_label;
        }

        public String getC_tt_id() {
            return c_tt_id;
        }

        public void setC_tt_id(String c_tt_id) {
            this.c_tt_id = c_tt_id;
        }

        public String getC_tt_label() {
            return c_tt_label;
        }

        public void setC_tt_label(String c_tt_label) {
            this.c_tt_label = c_tt_label;
        }

        @Override
        public String toString() {
            return "CTaxo{" +
                    "c_t_id='" + c_t_id + '\'' +
                    ", c_t_label='" + c_t_label + '\'' +
                    ", c_tt_parent_id='" + c_tt_parent_id + '\'' +
                    ", c_tt_parent_label='" + c_tt_parent_label + '\'' +
                    ", c_tt_id='" + c_tt_id + '\'' +
                    ", c_tt_label='" + c_tt_label + '\'' +
                    '}';
        }
    }

}
