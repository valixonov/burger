package com.example.user.scandoc.ui.main;

import com.example.user.scandoc.ui.base.MvpPresenter;

public interface MainMvpPresenter<V extends MainMvpView> extends MvpPresenter<V> {
    void logOut();
}
