package com.example.user.scandoc.ui.main;

import android.annotation.SuppressLint;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.user.scandoc.R;
import com.example.user.scandoc.ui.addItem.ItemActivity;
import com.example.user.scandoc.ui.base.BaseActivity;
//import com.example.user.scandoc.ui.doc_scanner.ScanDocActivity;
import com.example.user.scandoc.ui.login.LoginActivity;
import com.example.user.scandoc.ui.main.adapets.Callback;
import com.example.user.scandoc.ui.main.adapets.Item;
import com.example.user.scandoc.ui.main.adapets.ItemAdapter;
import com.example.user.scandoc.ui.main.adapets.MainAdapter;
import com.example.user.scandoc.ui.main.fragments.citizen.CitizenFragment;
import com.example.user.scandoc.ui.main.fragments.create_action.CreateActionFragment;
import com.example.user.scandoc.ui.main.fragments.dashboatd.DashboardFragment;
import com.example.user.scandoc.ui.main.fragments.mahalla.MahallaFragment;

import java.util.ArrayList;

public class MainActivity extends BaseActivity
        implements MainMvpView, NavigationView.OnNavigationItemSelectedListener, Callback, FragmentListener {

    MainMvpPresenter<MainMvpView> presenter;
    RecyclerView recyclerView;
    MainAdapter mainAdapter;
    ItemAdapter itemAdapter;
    Toolbar toolbar;
    MenuItem searchItem;
    MenuItem saveItem;

    Fragment fragment = null;
    Class fragmentClass = null;

    boolean isBack = false;
    ArrayList<Item> turkumItems, turkum;
    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();


    }


    @SuppressLint("RestrictedApi")
    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // toolbar.setLogo(R.drawable.ic_logout);


        presenter = new MainPresenter<>(this);
        presenter.onAttach(MainActivity.this);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //openItemActivity();
                fab_click();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        createItems();
        recyclerView = findViewById(R.id.recycler_view);
        mainAdapter = new MainAdapter(this, turkum);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(mainAdapter);
    }

    @SuppressLint("RestrictedApi")
    private void fab_click() {
        if (fragmentClass == CitizenFragment.class) {
//            showMessage("Create Fuqoro");
//            openItemActivity();
        } else {
            recyclerView.setVisibility(View.GONE);
            fab.setVisibility(View.GONE);
            saveItem.setVisible(true);
            searchItem.setVisible(false);
            openFragment(CreateActionFragment.class);
            setTitle(R.string.title_add_action);
        }
    }


    @SuppressLint("RestrictedApi")
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
            if (f instanceof CreateActionFragment) {
                searchItem.setVisible(true);
                saveItem.setVisible(false);
                openFragment(DashboardFragment.class);
                setTitle(R.string.title_activity_main_item);
                recyclerView.setVisibility(View.VISIBLE);
                fab.setVisibility(View.VISIBLE);
            } else if (isBack) {
                onBack();
            } else super.onBackPressed();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        searchItem = menu.findItem(R.id.action_settings);
        saveItem = menu.findItem(R.id.save_manu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView mSearchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_settings));
        final Toast toast = new Toast(this);

        if (mSearchView != null) {
            mSearchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
            mSearchView.setIconifiedByDefault(true);

            SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
                String mSearchString;

                public boolean onQueryTextChange(String newText) {
                    mSearchString = newText;
                    //doFilterAsync(mSearchString);
                    toast.makeText(getApplicationContext(), "Test1", Toast.LENGTH_LONG).show();
                    return true;
                }

                public boolean onQueryTextSubmit(String query) {
                    mSearchString = query;
                    //doFilterAsync(mSearchString);
                    toast.makeText(getApplicationContext(), "Test2", Toast.LENGTH_LONG).show();

                    return true;
                }
            };

            mSearchView.setOnQueryTextListener(queryTextListener);
        }


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressLint("RestrictedApi")
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_dashbord) {
            // Handle the camera action
            saveItem.setVisible(false);
            fab.setVisibility(View.GONE);
            recyclerView.setAdapter(mainAdapter);
            isBack = false;
            if (recyclerView.getVisibility() != View.VISIBLE)
                recyclerView.setVisibility(View.VISIBLE);
            fragmentClass = DashboardFragment.class;
        } else if (id == R.id.nav_mahhalla) {
            saveItem.setVisible(false);
            recyclerView.setVisibility(View.GONE);
            fab.setVisibility(View.GONE);
            fragmentClass = MahallaFragment.class;
        } else if (id == R.id.nav_citizens) {
            saveItem.setVisible(false);
            recyclerView.setVisibility(View.GONE);
            fab.setVisibility(View.VISIBLE);
            fragmentClass = CitizenFragment.class;
        } else if (id == R.id.Scan_doc) {
//            startActivity(new Intent(this, ScanDocActivity.class));
        } else if (id == R.id.nav_log_out) {
            openLoginActivity();
        }



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        if (fragmentClass != null){
            openFragment(fragmentClass);
        }
        setTitle(item.getTitle());

        return true;
    }

    @Override
    public void openLoginActivity() {
        presenter.logOut();
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    @Override
    public void openItemActivity() {
        startActivity(new Intent(this, ItemActivity.class));
    }

    @SuppressLint("RestrictedApi")
    private void onBack() {
        toolbar.setTitle(R.string.title_activity_main);
        recyclerView.setAdapter(mainAdapter);
        fab.setVisibility(View.INVISIBLE);
        isBack = false;
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void clickRecyclerItem(int i) {
        toolbar.setTitle(turkum.get(i).getTitle());
        itemAdapter = new ItemAdapter(turkumItems);
        recyclerView.setAdapter(itemAdapter);
        fab.setVisibility(View.VISIBLE);
        isBack = true;
    }

    @Override
    public Callback getCallback() {
        return this;
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public FloatingActionButton getFloatingActionButton() {
        return fab;
    }

    @Override
    public void openCitezenFragment() {
        openFragment(CitizenFragment.class);
        setTitle("Фуқаролар");
    }

    private void openFragment(Class fragmentClass) {
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction().replace(R.id.fragment_container, fragment).commit();
    }

    @Override
    public void changedToolbar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
    }

    private void createItems() {
        Item item = new Item("Низоли оилалар", "4 та чора тадбир туркумда 16та одам \n2 та тадбирнинг муддати яқинлашган", true);
        Item item1 = new Item("Руҳий касаллар", "10 та чора тадбир туркумда 35 та одам \n2 та тадбирнинг муддати яқинлашган", true);
        Item item2 = new Item("Тажовузкор характердаги", "12 та чора тадбир туркумда 40та одам \n2 та тадбирнинг муддати яқинлашган", false);
        Item item3 = new Item("Муқаддам судланганлар", "4 та чора тадбир туркумда 3та одам \n2 та тадбирнинг муддати яқинлашган", false);
        Item item4 = new Item("Спиртли ичимликка ружў қўйганлар", "4 та чора тадбир туркумда 16та одам \n2 та тадбирнинг муддати яқинлашган", false);
        Item item5 = new Item("Гиёҳвандликка ружў қўйганлар", "4 та чора тадбир туркумда 16та одам \n2 та тадбирнинг муддати яқинлашган", true);
        Item item6 = new Item("Носоғлом турмуш тарзини кечирувчи шахсла", "4 та чора тадбир туркумда 16та одам \n2 та тадбирнинг муддати яқинлашган", false);
        Item item7 = new Item("Қонуний никоҳдан ўтмасдан яшаётган шахслар", "4 та чора тадбир туркумда 16та одам \n2 та тадбирнинг муддати яқинлашган", false);
        Item item8 = new Item("Суицидга мойил шахслар", "4 та чора тадбир туркумда 16та одам \n2 та тадбирнинг муддати яқинлашган", false);
        Item item9 = new Item("Мулкини очиқда қолдирувчилар", "4 та чора тадбир туркумда 16та одам \n2 та тадбирнинг муддати яқинлашган", false);
        turkum = new ArrayList<>();
        turkum.add(item);
        turkum.add(item1);
        turkum.add(item2);
        turkum.add(item3);
        turkum.add(item4);
        turkum.add(item5);
        turkum.add(item6);
        turkum.add(item7);
        turkum.add(item8);
        turkum.add(item9);


        Item el = new Item("Шодиев Аскар, Норова гули", "Рашк \nҚўшимча: \nАдрес: Навоий кўчаси 8 уй", true);
        Item el1 = new Item("Собировлар", "Мерос \nҚўшимча: муқаддам судланган \nАдрес: Махтумқули 7 уй", true);
        Item el2 = new Item("Камолов Ботир, Турдиева Шодия", "Мол-мулк талашиш \nҚўшимча:Ажрашаетган ер \nАдрес:", false);
        Item el3 = new Item("Саидов Ж, Валиев Б", "Ер-Сув талашиш \nҚўшимча:қўшнилар \nАдрес: Низомий 7, Низомий 7", false);
        Item el4 = new Item("Ниёзова А, Жалилова А", "Мерос \nҚўшимча: Опа-сингил", false);
        turkumItems = new ArrayList<>();
        turkumItems.add(0, el);
        turkumItems.add(1, el1);
        turkumItems.add(2, el2);
        turkumItems.add(3, el3);
        turkumItems.add(4, el4);

    }


}
