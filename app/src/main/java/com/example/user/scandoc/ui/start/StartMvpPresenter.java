package com.example.user.scandoc.ui.start;

import com.example.user.scandoc.ui.base.MvpPresenter;

public interface StartMvpPresenter<V extends StartMvpView> extends MvpPresenter<V> {

    void isLogin();
}
