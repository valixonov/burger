package com.example.user.scandoc.data.network.model;

import java.util.List;

public class Action {

    private String a_id;
    private String a_status_id;
    private String a_date;
    private String a_timestamp;
    private String a_notes;
    private String a_t_count;
    private String a_citezen_count;
    private List<ActionLocatoin> a_locations;
    private ArticleType a_article_types;
    private ActionHistory a_history;
    private List<Citizen> a_citizens;
    private List<ActionTaxo> a_t;

    public Action() {
    }

    public String getA_id() {
        return a_id;
    }

    public void setA_id(String a_id) {
        this.a_id = a_id;
    }

    public String getA_status_id() {
        return a_status_id;
    }

    public void setA_status_id(String a_status_id) {
        this.a_status_id = a_status_id;
    }

    public String getA_date() {
        return a_date;
    }

    public void setA_date(String a_date) {
        this.a_date = a_date;
    }

    public String getA_timestamp() {
        return a_timestamp;
    }

    public void setA_timestamp(String a_timestamp) {
        this.a_timestamp = a_timestamp;
    }

    public String getA_notes() {
        return a_notes;
    }

    public void setA_notes(String a_notes) {
        this.a_notes = a_notes;
    }

    public String getA_t_count() {
        return a_t_count;
    }

    public void setA_t_count(String a_t_count) {
        this.a_t_count = a_t_count;
    }

    public String getA_citezen_count() {
        return a_citezen_count;
    }

    public void setA_citezen_count(String a_citezen_count) {
        this.a_citezen_count = a_citezen_count;
    }

    public List<ActionLocatoin> getA_locations() {
        return a_locations;
    }

    public void setA_locations(List<ActionLocatoin> a_locations) {
        this.a_locations = a_locations;
    }

    public ArticleType getA_article_types() {
        return a_article_types;
    }

    public void setA_article_types(ArticleType a_article_types) {
        this.a_article_types = a_article_types;
    }

    public ActionHistory getA_history() {
        return a_history;
    }

    public void setA_history(ActionHistory a_history) {
        this.a_history = a_history;
    }

    public List<Citizen> getA_citizens() {
        return a_citizens;
    }

    public void setA_citizens(List<Citizen> a_citizens) {
        this.a_citizens = a_citizens;
    }

    public List<ActionTaxo> getA_t() {
        return a_t;
    }

    public void setA_t(List<ActionTaxo> a_t) {
        this.a_t = a_t;
    }

    @Override
    public String toString() {
        return "Action{" +
                "a_id='" + a_id + '\'' +
                ", a_status_id='" + a_status_id + '\'' +
                ", a_date='" + a_date + '\'' +
                ", a_timestamp='" + a_timestamp + '\'' +
                ", a_notes='" + a_notes + '\'' +
                ", a_t_count='" + a_t_count + '\'' +
                ", a_citezen_count='" + a_citezen_count + '\'' +
                ", a_locations=" + a_locations +
                ", a_article_types=" + a_article_types +
                ", a_history=" + a_history +
                ", a_citizens=" + a_citizens +
                ", a_t=" + a_t +
                '}';
    }
}
