package com.example.user.scandoc.ui.login;

import com.example.user.scandoc.ui.base.MvpView;

public interface LoginMvpView extends MvpView {
    void openMainActivity();
}
