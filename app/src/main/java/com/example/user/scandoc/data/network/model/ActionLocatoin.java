package com.example.user.scandoc.data.network.model;

public class ActionLocatoin {
    private String lat;
    private String lon;
    private String timestamp;

    public ActionLocatoin() {
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "ActionLocatoin{" +
                "lat='" + lat + '\'' +
                ", lon='" + lon + '\'' +
                ", timestamp='" + timestamp + '\'' +
                '}';
    }
}
