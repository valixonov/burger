package com.example.user.scandoc.data.network.model;

public class TurkumList {
    private String date1;
    private int taxonomy_id;
    private int count;
    private int action1;
    private String taxonomy;

    public String getDate1() {
        return date1;
    }

    public void setDate1(String date1) {
        this.date1 = date1;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getAction1() {
        return action1;
    }

    public void setAction1(int action) {
        this.action1 = action1;
    }

    public String getTaxonomy() {
        return taxonomy;
    }

    public void setTaxonomy(String taxonomy) {
        this.taxonomy = taxonomy;
    }

    public int getTaxonomy_id() {
        return taxonomy_id;
    }

    public void setTaxonomy_id(int taxonomy_id) {
        this.taxonomy_id = taxonomy_id;
    }
}
