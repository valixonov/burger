package com.example.user.scandoc.ui.main.fragments.create_action;

import com.example.user.scandoc.ui.base.MvpPresenter;
import com.example.user.scandoc.ui.base.MvpView;

public interface CreateActionMvpPresenter<V extends MvpView> extends MvpPresenter<V> {
}
