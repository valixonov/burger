package com.example.user.scandoc.data.network;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.example.user.scandoc.data.network.ApiEndPoint;
import com.example.user.scandoc.data.network.ApiService;
import com.example.user.scandoc.data.network.model.TurkumList;
import com.example.user.scandoc.data.network.model.User;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    private Context context;
    private String respon;
    private List<User> users;
    private List<TurkumList> turkumLists;

    public ApiClient(Context context) {
        this.context = context;
    }

    public List<User> getUsers() {

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("https://api.github.com/")
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();

        ApiService client = retrofit.create(ApiService.class);
        Call<List<User>> call = client.reposForUser("fs-opensource");

        call.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                List<User> repos = response.body();
                Log.d("logger", "onResponse: " + repos.toString());
                System.out.println("======================= response");
                //listView.setAdapter(new GitHupRepoAdepter(this, repos));
               users = repos;
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                Toast.makeText(context, "Error", Toast.LENGTH_LONG).show();
                System.out.println("======================= error");
            }
        });

        return users;
    }

    public List<TurkumList> getTurkumLists(){

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();



        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://192.168.1.112:4000/taxonomy/")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiService service = retrofit.create(ApiService.class);
        Call<List<TurkumList>> call = service.reposForTur();

        call.enqueue(new Callback<List<TurkumList>>() {
            @Override
            public void onResponse(Call<List<TurkumList>> call, Response<List<TurkumList>> response) {
                Log.d("myRes", "onResponse: " + response.toString());
                turkumLists = response.body();
                System.out.println("============== 78 yes");
            }

            @Override
            public void onFailure(Call<List<TurkumList>> call, Throwable t) {
                Log.d("myRes", "onFailure: Error");
                System.out.println("============== 84 no" + call.toString() + " \t " + t.getMessage());
            }
        });

        return turkumLists;
    }
}
