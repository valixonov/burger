package com.example.user.scandoc.ui.main.fragments.citizen;

import com.example.user.scandoc.ui.base.MvpPresenter;


public interface CitezenMvpPresenter<V extends CitezenMvpView> extends MvpPresenter<V> {
}
