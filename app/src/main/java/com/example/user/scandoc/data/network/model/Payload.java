package com.example.user.scandoc.data.network.model;

import java.util.List;

public class Payload {

    private String rft_id;
    private String rft_label;
    private Stats rft_stats;
    private List<Action> rft_actions;
}
