package com.example.user.scandoc.ui.main;

import android.content.Context;

import com.example.user.scandoc.ui.base.BasePresenter;

public class MainPresenter<V extends MainMvpView> extends BasePresenter<V> implements MainMvpPresenter<V> {

    public MainPresenter(Context context) {
        super(context);
    }

    @Override
    public void logOut() {
        getDataManager().setLogOut();
    }
}
