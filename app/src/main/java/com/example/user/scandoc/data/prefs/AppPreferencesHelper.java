package com.example.user.scandoc.data.prefs;

import android.content.Context;
import android.content.SharedPreferences;

public class AppPreferencesHelper implements PreferencesHelper{

    SharedPreferences mPref;
    public AppPreferencesHelper(Context context){
        mPref = context.getSharedPreferences("users", Context.MODE_PRIVATE);
    }
    @Override
    public void setLoggedIn() {
        mPref.edit().putBoolean("login", true).apply();
    }

    @Override
    public boolean isLoggedIn() {
        return mPref.getBoolean("login", false);
    }

    @Override
    public void setLogOut() {
        mPref.edit().putBoolean("login", false).apply();
    }
}
